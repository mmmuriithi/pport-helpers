<?php
namespace pPort\Helpers;
/**
 * pPort Str Utility
 *
 * Provides various functions to interact/manipulate strings
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.utils
 * @category framework
 * @link http://www.pporting.org/docs/system/utils/string
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

class Str
{

    /**
    *
    * Shorthand to var_dump and die.Use this instead of var_dump($var);die();
    *
    * @access public
    * @param <mixed> $var <The variable to dump>
    */
    public static function vd($var)
    {
        var_dump($var);
        die();
    }


    /**
    *
    * Shorthand to print and die.Use this instead of die(print_r($var));
    *
    * @access public
    * @param <mixed> $var <The variable to print>
    */

        function pd($var)
        {
            print_r($var);
            die();
        }


    /**
    *
    * Shorthand to print.Use this instead of print_r($var);
    * @param <mixed> $var <The variable to print>
    */

        function p($var)
        {
            print_r($var);
        }



   
        public static function v($var=FALSE)
        {
            var_dump($var);
        }
    

    
        public static function d($var=FALSE)
        {
            die(var_dump($var));
        }
    


    
        public static function eco($cond, $cond_true, $cond_default = "")
        {
            echo ($cond) ? $cond_true : $cond_default;
        }
    

public static function alphabets()
{
    return range('A', 'Z');
}

public static function substr_name()
{
    if($type==FALSE)
        {
            $substred_name=substr($name,0,20);
            if($substred_name && $substred_name=!$name)
            {
                return substr($name,0,20)."...";
            }
            else
            {
                return $name;
            }
        }
}

public static function to_u($string,$search=' ')
    {
        return strtolower(str_replace($search,'_',$string));
    }

public static function pad_with_string($data_array,$into=["['","']"])
    {
        $string="";
        foreach($data_array as $key)
        {
            $string.=$into[0].$key.$into[1];
        }
        return $string;
    }

public static function scrape_and_group($string,$params,$params_children)
{
    $separator = "\r\n";
    //$line = strtok($string, $separator);
    $lines=preg_split ('/$\R?^/m', $string);
    $count=0;
    
    $extracted_lines=array();
    $extracted_children_lines=array();
    $parent_childrens=array();
    $parent_count=0;
    $children_count=0;
    foreach($lines as $line)
    {
        foreach($params as $param)
        {
            foreach($param[1] as $replacers)
            {
                $extracted=($extracted=between($line,$replacers[0],$replacers[1]))?$extracted:false;
                if($extracted)
                {
                    //Add the result parent to parents array
                    break;
                }

            }
            $result=isset($param[2])?$param[2]($line,$extracted):$extracted;
            //Add the result parent
            if($result)
            {

                $extracted_children_lines=array();
                $children_count=0;
                $parent_count++;
                $extracted_lines[$count][$param[0]]=$result;

                $parent_childrens[$parent_count]['parent'][$count][$param[0]]=$result;
                continue;
            }

        }

        //If Has Parent
        if($parent_count>0)
        {

            foreach($params_children as $param)
            {

                foreach($param[1] as $replacers)
                {
                    $extracted=($extracted=between($line,$replacers[0],$replacers[1]))?$extracted:false;

                    if($extracted)
                    {
                        //Add the result parent to parents array
                        break;
                    }

                }
                $result=isset($param[2])?$param[2]($line,$extracted):$extracted;
                //Add the result parent

                if($result)
                {
                    $extracted_children_lines[$count][$param[0]]=$result;
                    $parent_childrens[$parent_count]['children'][$children_count][$param[0]]=$result;
                    $children_count++;
                    continue;

                }
            }
        }
        


        $count++;
    }

    return $parent_childrens;
}

    public static function scrape_from()
    {
        $separator = "\r\n";
        //$line = strtok($string, $separator);
        $lines=preg_split ('/$\R?^/m', $string);
        $count=0;
        
        $extracted_lines=array();
        foreach($lines as $line)
        {
            foreach($params as $param)
            {
                foreach($param[1] as $replacers)
                {
                    $extracted=($extracted=between($line,$replacers[0],$replacers[1]))?$extracted:false;
                    if($extracted)
                    {
                        break;
                    }

                }
                $result=isset($param[2])?$param[2]($line,$extracted):$extracted;
                if($result)
                {
                    $extracted_lines[$count][$param[0]]=$result;

                }

            }
            $count++;
        }

    return $extracted_lines;
    }

    /**
     *
     *Checks if a string is a url
     *
     *@param     <string> $string <The string to check>
     *@return    <boolean>
     *
     */
    public static function is_url($string)
    {
        if(filter_var(trim($string), FILTER_VALIDATE_URL))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    /**
     *
     *Returns a string  with url's parsed and returned as links
     *
     *@param     <string>    directory where all algorithms are located
     *@return    <array>     supported ciphers
     *
     */
    public static function with_urls($text,$attrs=array())
    {
        $reg_exUrl ="/((((http|https|ftp|ftps)\:\/\/)|www\.)[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,4}(\/\S*)?)/";
        if(is_array($attrs)&&!empty($attrs))
        {
            array_walk($attrs, create_function('&$i,$k','$i=" $k=\"$i\"";'));
            $attrs = implode($attrs,"");
        }
        else
        {
            $attrs="";
        }
        return preg_replace( $reg_exUrl,"<a href=\"$1\" ".$attrs.">$1</a>", $text );
    }

    /**
     *
     *Returns a syntax highlighted version of the given PHP
     *
     *@param     <string> $string <The php code string to highlight>
     *@return    <string> <The highlighted version of the php code>
     *
     */
    public static function highlight_php($string)
    {
        return highlight_string($string);
    }

    /**
     *
     *Returns a count of the words in a string
     *
     *@param     <string> $string <The string to count>
     *@return    <int> <The number of words in the string>
     *
     */
    public static function count_words($string)
    {
        return str_word_count($string);
    }

    /**
     *
     *Tells the difference in two strings
     *
     *@param     <string> $string1 <The first string>
     *@param     <string> $string2 <The string to check against>
     *@return    <int> <The character difference in the two words>
     *
     */
    public static function difference($string1,$string2)
    {
        return levenshtein($string1,$string2);
    }

    /**
     *
     *Returns the PHP source code in filename without PHP comments and whitespace removed.
     *
     *@param     <string> $file_name <The php file>
     *@return    <int> <The php source code in filename without PHP comments and whitespace >
     *
     */
    public static function php_strip($file_name)
    {
        return php_strip_whitespace($file_name);
    }

    /**
     *
     *Returns a string  between two specified characters in the string
     *
     *@param  <string> $content <The string to check>
     *@param  <string> $start <The first character>
     *@param  <string> $end <The second character>
     *@return <string> <The extracted string>
     *
     */
    public static function between($string,$start,$end)
    {
        $r = explode($start,$string);
        if (isset($r[1]))
        {
            $r = explode($end, $r[1]);
            return $r[0];
        }
        return '';
    }

    /**
     *
     *Extracts emails from a string
     *
     *@param  <string> $string <The string to check>
     *@return <array> <Array of extracted emails>
     *
     */
    public static function extract_emails($string)
    {
        $regexp = '/([a-z0-9_\.\-])+\@(([a-z0-9\-])+\.)+([a-z0-9]{2,4})+/i';
        preg_match_all($regexp, $string, $m);
        return isset($m[0]) ? $m[0] : array();
    }

    public static function starts_with($haystack, $needle)
    {
        $length = strlen($needle);
        return (substr($haystack, 0, $length) === $needle);
    }

    public static function ends_with($haystack, $needle)
    {
        return substr($haystack, -strlen($needle))===$needle;
    }

    public static function to_254($phone)
    {
        $phone=ltrim($phone,"+");
        if(Str::starts_with($phone,'7'))
        {
            $phone=substr_replace($phone, "254", 0,0);
        }

        return Str::starts_with($phone,'254')?$phone:substr_replace($phone, "254", 0, 1);
    }

    public static function has($stack,$string)
    {
        if(strrpos($stack,$string)!==false)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static function truncate($string, $width, $etc = ' ..')
    {
        $wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
        return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
    }

    public static function truncate_words($string,$words=20)
    {
        return preg_replace('/((\w+\W*){'.($words-1).'}(\w+))(.*)/', '${1}', $string);
    }

    public static function to_words($number) {

        $hyphen      = '-';
        $conjunction = ' and ';
        $separator   = ', ';
        $negative    = 'negative ';
        $decimal     = ' point ';
        $dictionary  = array(
            0                   => 'zero',
            1                   => 'one',
            2                   => 'two',
            3                   => 'three',
            4                   => 'four',
            5                   => 'five',
            6                   => 'six',
            7                   => 'seven',
            8                   => 'eight',
            9                   => 'nine',
            10                  => 'ten',
            11                  => 'eleven',
            12                  => 'twelve',
            13                  => 'thirteen',
            14                  => 'fourteen',
            15                  => 'fifteen',
            16                  => 'sixteen',
            17                  => 'seventeen',
            18                  => 'eighteen',
            19                  => 'nineteen',
            20                  => 'twenty',
            30                  => 'thirty',
            40                  => 'fourty',
            50                  => 'fifty',
            60                  => 'sixty',
            70                  => 'seventy',
            80                  => 'eighty',
            90                  => 'ninety',
            100                 => 'hundred',
            1000                => 'thousand',
            1000000             => 'million',
            1000000000          => 'billion',
            1000000000000       => 'trillion',
            1000000000000000    => 'quadrillion',
            1000000000000000000 => 'quintillion'
        );

        if (!is_numeric($number)) {
            return false;
        }

        if (($number >= 0 && (int) $number < 0) || (int) $number < 0 - PHP_INT_MAX) {
            // overflow
            trigger_error(
                'convert_number_to_words only accepts numbers between -' . PHP_INT_MAX . ' and ' . PHP_INT_MAX,
                E_USER_WARNING
            );
            return false;
        }

        if ($number < 0) {
            return $negative . convert_number_to_words(abs($number));
        }

        $string = $fraction = null;

        if (strpos($number, '.') !== false) {
            list($number, $fraction) = explode('.', $number);
        }

        switch (true) {
            case $number < 21:
                $string = $dictionary[$number];
                break;
            case $number < 100:
                $tens   = ((int) ($number / 10)) * 10;
                $units  = $number % 10;
                $string = $dictionary[$tens];
                if ($units) {
                    $string .= $hyphen . $dictionary[$units];
                }
                break;
            case $number < 1000:
                $hundreds  = $number / 100;
                $remainder = $number % 100;
                $string = $dictionary[$hundreds] . ' ' . $dictionary[100];
                if ($remainder) {
                    $string .= $conjunction . convert_number_to_words($remainder);
                }
                break;
            default:
                $baseUnit = pow(1000, floor(log($number, 1000)));
                $numBaseUnits = (int) ($number / $baseUnit);
                $remainder = $number % $baseUnit;
                $string = convert_number_to_words($numBaseUnits) . ' ' . $dictionary[$baseUnit];
                if ($remainder) {
                    $string .= $remainder < 100 ? $conjunction : $separator;
                    $string .= convert_number_to_words($remainder);
                }
                break;
        }

        if (null !== $fraction && is_numeric($fraction)) {
            $string .= $decimal;
            $words = array();
            foreach (str_split((string) $fraction) as $number) {
                $words[] = $dictionary[$number];
            }
            $string .= implode(' ', $words);
        }

        return $string;
    }
}
?>