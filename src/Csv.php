<?php
namespace pPort\Helpers;
/**
 * pPort Str Utility
 *
 * Provides various functions to interact/manipulate strings
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.utils
 * @category framework
 * @link http://www.pporting.org/docs/system/utils/string
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

class Csv
{
    public static function to_csv($all_results,$optional_name='data_export')
    {
        foreach( $all_results as $row )
            {
                $line = '';
                foreach( $row as $value )
                {                                            
                    if ( ( !isset( $value ) ) || ( $value == "" ) )
                    {
                        $value = "\t";
                    }
                    else
                    {
                        $value = str_replace( '"' , '""' , $value );
                        $value = '"' . $value . '"' . "\t";
                    }
                    $line .= $value;
                }
                $data .= trim( $line ) . "\n";
            }
            $data = str_replace( "\r" , "" , $data );

            if ( $data == "" )
            {
                $data = "\n(0) Records Found!\n";                        
            }

            header("Content-type: application/octet-stream");
            header("Content-Disposition: attachment; filename=".$optional_name.".xls");
            header("Pragma: no-cache");
            header("Expires: 0");
            print "\n$data";
    }
    
}
?>