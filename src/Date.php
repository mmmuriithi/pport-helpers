<?php
namespace pPort\Helpers;
/**
 * pPort Str Utility
 *
 * Provides various functions to interact/manipulate strings
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.utils
 * @category framework
 * @link http://www.pporting.org/docs/system/utils/string
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

class Date
{
    public static function get_quotient_and_remainder($date)
    {
         //$date1 = new DateTime('2014-06-03 07:21:00');
        $date1 = is_object($date)?$date:new DateTime($date);
        $date2 = new DateTime("now");
        $interval = $date1->diff($date2);
        $years = $interval->format('%y');
        $months = $interval->format('%m');
        $days = $interval->format('%d');
        $hours = $interval->format('%H');
        $minutes = $interval->format('%i');
        $seconds = $interval->format('%s');
        if($years!=0)
        {
            if($years>1)
            {
                $ago = $years.' years ago'; 
            } 
            else
            {
                $ago = $years.' year ago';
            }           
        }
        elseif($months!=0)
        {
            if($months>1)
            {
                $ago =$months.' months ago';
            }
            else
            {
                $ago =$months.' month ago';
            }           
        }
        elseif($days!=0)
        {
            if($days>1)
            {
                $ago=$days.' days ago'; 
            }
            else
            {
                $ago=$days.' day ago';
            }           
        }
        elseif($minutes!=0)
        {
            if($minutes>1)
            {
                $ago=$minutes.' minutes ago';
            }
            else
            {
                $ago=$minutes.' minute ago';
            }           
        }
        elseif($seconds!=0)
        {
            if($seconds>1)
            {
                $ago=$seconds.' seconds ago';
            }
            else
            {
                $ago=$seconds.' second ago';
            }           
        }
        else
        {
            $ago="1 second ago";
        }
        return $ago;   
    }
    
}
?>