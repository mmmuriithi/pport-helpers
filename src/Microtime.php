<?php 
namespace pPort\Helpers;

class Microtime
{
    /**
*marktime utility
*
*utility to find code execution time
*
*@category  utils
*@package   pPort_system.utils
*@author    Martin Muriithi <mmmuriithi@yahoo.com>
*@link      http://www.pport.net
*@license   http://www.pport.net/license
*@since     version 1.0
*/


/**
*Gets content from a csv file
*
*@access  public
*@return  <int>   $start Result of start time
*/
public static function start()
{
    $time = microtime();
    $time = explode(' ', $time);
    $time = $time[1] + $time[0];
    $start = $time;
    return $start;
}

/**
*Gets content from a csv file
*
*@access  public
*@return  <int>   $finish Result of end time
*/
public static function finish()
{
    return static::start();
}

/**
*Gets content from a csv file
*
*@access  public
*@param   <int>   $start Start time gotten via start()
*@return  <int>   $finish Finish time gotten via finish()
*/
public static function result($start,$end,$message=NULL)
{
    if($message)
    {
        $total_time = 'executed in '.round(($end - $start), 4).' microseconds';
        return $total_time;
    }
    $total_time = ($end - $start);
    return $total_time;
}
}

?>