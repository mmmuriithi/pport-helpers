<?php
use pPort\Helpers\Str;
use pPort\Helpers\Arr;
use pPort\Helpers\Date;
use pPort\Helpers\Geo;
use pPort\Helpers\Math;
use pPort\Helpers\Microtime;

/**
 * Php Functions File
 *
 * Implements shorthand functions for executing common php functions.
 *
 * NOTE : You can add more of your functions here.
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.framework.functions
 * @category framework
 * @link http://www.pporting.org/docs/system/framework/includes/functions
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

/**
*
* Shorthand to var_dump and die.Use this instead of var_dump($var);die();
*
* @access public
* @param <mixed> $var <The variable to dump>
*/
if(!function_exists("vd"))
{
	function vd($var)
	{
		Str::vd($var);
	}
}

/**
*
* Shorthand to print and die.Use this instead of die(print_r($var));
*
* @access public
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("pd"))
{
	function pd($var)
	{
		Str::pd($var);
	}
}

/**
*
* Shorthand to print.Use this instead of print_r($var);
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("p"))
{
	function p($var)
	{
		Str::p($var);
	}
}

/**
*
* Shorthand to var_dump.Use this instead of var_dump($var);
*
* @access public
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("v"))
{
	function v($var=FALSE)
	{
		Str::v($var);
	}
}

/**
*
* Shorthand to die.Use this instead of die(var_dump($var)) or die() alone;
*
* @access public
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("d"))
{
	function d($var=FALSE)
	{
		Str::d($var);
	}
}

/**
 *
 * Shorthand to die.Use this instead of die(var_dump($var)) or die() alone;
 *
 * @access public
 * @param <mixed> $var <The variable to print>
 */
if (!function_exists("eco")) {
    function eco($cond, $cond_true, $cond_default = "")
    {
        Str::eco($cond, $cond_true, $cond_default);
    }
}

/**
 *
 * Shorthand to print.Use this instead of print_r($var);
 * @param <mixed> $var <The variable to print>
 */
if (!function_exists("caller")) {
    function caller()
    {

        //get the trace
        $trace = debug_backtrace();

        // Get the class that is asking for who awoke it
        $class = $trace[1]['class'];

        // +1 to i cos we have to account for calling this function
        for ($i = 1; $i < count($trace); $i++) {
            if (isset($trace[$i])) // is it set?
                if ($class != $trace[$i]['class']) // is it a different class
                    return $trace[$i]['class'];
        }
    }
}

/**
 *
 * Shorthand to die.Use this instead of die(var_dump($var)) or die() alone;
 *
 * @access public
 * @param <mixed> $var <The variable to print>
 */
if (!function_exists("url")) {
    function url($val = '')
    {
        return Url::base($val);
    }
}

function shorten($text, $char)
{
    $text = substr($text, 0, $char); //First chop the string to the given character length
    if (substr($text, 0, strrpos($text, ' ')) != '') $text = substr($text, 0, strrpos($text, ' ')); //If there exists any space just before the end of the chopped string take upto that portion only.
    //In this way we remove any incomplete word from the paragraph
    $text = $text . '...'; //Add continuation ... sign
    return $text; //Return the value
}

function fetch_image($img_url, $store_dir = 'image', $store_dir_type = 'relative', $overwrite = false, $pref = false, $debug = false)
{
    //first get the base name of the image
    $i_name = explode('.', basename($img_url));
    $i_name = $i_name[0];

    //now try to guess the image type from the given url
    //it should end with a valid extension...
    //good for security too
    if (preg_match('/https?:\/\/.*\.png$/i', $img_url)) {
        $img_type = 'png';
    } else if (preg_match('/https?:\/\/.*\.(jpg|jpeg)$/i', $img_url)) {
        $img_type = 'jpg';
    } else if (preg_match('/https?:\/\/.*\.gif$/i', $img_url)) {
        $img_type = 'gif';
    } else {
        if (true == $debug)
            echo 'Invalid image URL';
        return ''; //possible error on the image URL
    }

    $dir_name = (($store_dir_type == 'relative') ? './' : '') . rtrim($store_dir, '/') . '/';

    //create the directory if not present
    if (!file_exists($dir_name))
        mkdir($dir_name, 0777, true);

    //calculate the destination image path
    $i_dest = $dir_name . $i_name . (($pref === false) ? '' : '_' . $pref) . '.' . $img_type;

    //lets see if the path exists already
    if (file_exists($i_dest)) {
        $pref = (int)$pref;

        //modify the file name, do not overwrite
        if (false == $overwrite)
            return itg_fetch_image($img_url, $store_dir, $store_dir_type, $overwrite, ++$pref, $debug);
        //delete & overwrite
        else
            unlink($i_dest);
    }

    //first check if the image is fetchable
    $img_info = @getimagesize($img_url);

    //is it a valid image?
    if (false == $img_info || !isset($img_info[2]) || !($img_info[2] == IMAGETYPE_JPEG || $img_info[2] == IMAGETYPE_PNG || $img_info[2] == IMAGETYPE_JPEG2000 || $img_info[2] == IMAGETYPE_GIF)) {
        if (true == $debug)
            echo 'The image doesn\'t seem to exist in the remote server';
        return ''; //return empty string
    }

    //now try to create the image
    if ($img_type == 'jpg') {
        $m_img = @imagecreatefromjpeg($img_url);
    } else if ($img_type == 'png') {
        $m_img = @imagecreatefrompng($img_url);
        @imagealphablending($m_img, false);
        @imagesavealpha($m_img, true);
    } else if ($img_type == 'gif') {
        $m_img = @imagecreatefromgif($img_url);
    } else {
        $m_img = FALSE;
    }

    //was the attempt successful?
    if (FALSE === $m_img) {
        if (true == $debug)
            echo 'Can not create image from the URL';
        return '';
    }

    //now attempt to save the file on local server
    if ($img_type == 'jpg') {
        if (imagejpeg($m_img, $i_dest, 100))
            return $i_dest;
        else
            return '';
    } else if ($img_type == 'png') {
        if (imagepng($m_img, $i_dest, 0))
            return $i_dest;
        else
            return '';
    } else if ($img_type == 'gif') {
        if (imagegif($m_img, $i_dest))
            return $i_dest;
        else
            return '';
    }

    return '';
}

/**
*
* Shorthand to die.Use this instead of die(var_dump($var)) or die() alone;
*
* @access public
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("resolve_portlet"))
{
	function resolve_portlet($pt_name=FALSE)
	{
		if(strpos($pt_name,"::")!==FALSE)
		{
			return str_replace("::","/",$pt_name);
		}
		else
		{
			return $name=FALSE;
		}
	}
}

/**
*
* Parses a name and converts it to a portlet compatible segment
*
* @access public
* @param <mixed> $var <The variable to print>
*/
if(!function_exists("to_portlet"))
{
	function to_portlet($pt_name,$segmented=FALSE)
	{
		if(is_string($pt_name))
		{
			if(strpos($pt_name,"::")!==FALSE)
			{
				return ($segmented===TRUE)?explode("::",$pt_name):str_replace("::","/",$pt_name);
			}
		}
		else
		{
			return FALSE;
		}
	}
}

/**
 * Convert an array of keys=>value to key="value" attributes;
 *
 * @access public
 * @param <string> $attributes <The string of constructed attributes>
 */
if (!function_exists("build_attributes")) {
    function build_attributes($attributes,$result=[])
    {
        if (is_array($attributes)) {
            $build_attributes=array();
            array_walk($attributes, function ($i, $k) use(&$build_attributes,$result) {
                if(!is_array($i) AND !is_object($i))
                {
                    $attr_vals = between_all($i, "{@", "}");

                        if (!empty($attr_vals))
                        {
                            foreach ($attr_vals as $attr_k => $attr_v)
                            {
                                $replace_val=is_object($result)&&isset($result->{$attr_v})?$result->{$attr_v}:arr($attr_v,$result,false);
                                if($replace_val)
                                {
                                    $i = str_replace('{@' . $attr_v. '}',$replace_val, $i);
                                }
                                
                            }
                        }
                    

                }
                elseif(is_closure($i))
                {
                    $i=$i($result);
                }

                $i = (is_pport_field_attribute($k)) ? "" : $k . '="' . $i . '" ';
                $build_attributes[$k]=$i;

            });
            $attributes = implode($build_attributes, "");
        } else {
            $attributes = "";
        }
        return $attributes;
    }
}

if (!function_exists("extract_attributes")) {
	function extract_attributes($str_attributes,$separator=' ',$key_separator='=')
	{
		$extracted_attrs=explode($separator,$str_attributes);
		$attributes=array();
		foreach($extracted_attrs as $k=>$attribute)
		{
			if(strpos($attribute,$key_separator)!==false)
			{
				$attribute=explode('=',$attribute);
				$attributes[$attribute[0]]=$attribute[1];
			}
		}
		return $attributes;
	}
}

/**
* Check if an array is an associative array;
*
* @access public
* @param <array> $array <The array to check>
*/
if(!function_exists("is_assoc"))
{
	function is_assoc(array $array)
	{
	    // Keys of the array
	    $keys = array_keys($array);
	    // If the array keys of the keys match the keys, then the array isn't associative
	    return array_keys($keys) !== $keys;
	}
}

/**
* Fetch string between two characters,be sure that the string format is consistent;
*
* @access public
* @param <input> $array <The array to check>
*/
if(!function_exists("between"))
{
	function between($string,$char_1="{",$char_2="}")
	{
		preg_match('~'.$char_1.'(.*?)'.$char_2.'~', $string, $output);
	    return isset($output[1])?$output[1]:false;
	}
}

/**
* Replace string between two characters,
*
* @access public
* @param <input> $array <The array to check>
*/
if(!function_exists("between_replace"))
{
	function between_replace($string,$replacement="",$char_1="{",$char_2="}",$all=TRUE)
	{
		if($all===TRUE)
		{
			return preg_replace('~'.$char_1.'(.*?)'.$char_2.'~', $replacement, $string);
		}
		else
		{
			return preg_replace('~'.$char_1.'(.*?)'.$char_2.'~', $replacement, $string,$all);
		}	   	
	}
}

/**
* Fetch string between two characters,be sure that the string format is consistent;
*
* @access public
* @param <input> $array <The array to check>
*/
if(!function_exists("between_matches"))
{
	function between_all($string,$char_1="{",$char_2="}")
	{
        
	   	preg_match_all('~'.$char_1.'(.*?)'.$char_2.'~', $string, $output);

	    return isset($output[1])?$output[1]:'';
	}
}

/**
* Check if string exists in another string;
*
* @access public
* @param <string> $string <The string to search in>
* @param <string> $search <The string to search for>
*/
if(!function_exists("in_string"))
{
	function in_string($search,$string)
	{
	   	return (strrpos($string, $search)!==FALSE)?TRUE:FALSE;
	}
}

/**
*
* Shorthand to convert array to objects.Works for even multi-dimensional arrays
* @param <array> $array <The array to be converted>
*/
if(!function_exists("to_object"))
{
	function to_object($array) {
	    if (!is_array($array)) {
	        return $array;
	    }
	    
	    $object = new stdClass();
	    if (is_array($array) && count($array) > 0)
	    {
	        foreach ($array as $name=>$value) 
	        {
	            $name = strtolower(trim($name));
	            if (!is_null($name)) 
	            {
	                $object->$name = a_to_o($value);
	            }
	        }
	        return $object;
	    }
	    else {
	        return FALSE;
	    }
	}
}


/**
*
* Recursively convert any php object into an array
*
* @param <obj> $obj <Object to be converted>
*/
if(!function_exists("to_array"))
{
	function to_array($obj) 
	{
		if(is_object($obj)) $obj = (array) $obj;
		if(is_array($obj)) 
		{
			$new = array();
			foreach($obj as $key => $val) 
			{
				$new[$key] = to_array($val);
			}
		}
		else 
		{
			$new = $obj;
		}		
		return $new;
	}
}

/**
 *
 * Get the specified duration in the future from the specified date
 *
 * @param <int> $duration <The duration>
 * @param <string> $unit <The unit of the duration i.e. days,hours,year>
 * @param <string> $from_date <The date to check from>
 */
if (!function_exists("the_future")) {
    function the_future($duration = "1", $unit = "days", $from_date = FALSE, $format = 'Y-m-d H:i:s')
    {
        $from_date = ($from_date !== FALSE) ? $from_date : date('Y-m-d H:i:s');
        return $date = date($format, strtotime($from_date . ' +' . $duration . ' ' . $unit));
    }
}

/**
 *
 * Get the specified duration in the past from the specified date
 *
 * @param <int> $duration <The duration>
 * @param <string> $unit <The unit of the duration i.e. days,hours,year>
 * @param <string> $from_date <The date to check from>
 */
if (!function_exists("the_past")) {
    function the_past($duration = "1", $unit = "day", $from_date = FALSE, $format = 'Y-m-d')
    {
        $from_date = ($from_date !== FALSE) ? $from_date : date("Y-m-d H:i:s");
        return $date = date($format, strtotime($from_date . ' -' . $duration . ' ' . $unit));
    }
}

/**
 *
 * Get the specified duration in the past from the specified date
 *
 * @param <int> $duration <The duration>
 * @param <string> $unit <The unit of the duration i.e. days,hours,year>
 * @param <string> $from_date <The date to check from>
 */
if (!function_exists("the_difference")) {
    function the_difference($date1, $date2)
    {
        $date1 = date_create($date1);
        $date2 = date_create($date2);
        $diff = date_diff($date1, $date2);

        return $diff->format("%R%a");
    }
}


/**
 *
 * Get the specified duration in the past from the specified date
 *
 * @param <int> $duration <The duration>
 * @param <string> $unit <The unit of the duration i.e. days,hours,year>
 * @param <string> $from_date <The date to check from>
 */
if (!function_exists("the_diff")) {
    function the_diff($date1, $date2,$in='days')
    {
        $dateFrom = new DateTime($date1); 
        $dateTo = new DateTime($date2); 
        $difference_time=$dateTo->getTimestamp()-$dateFrom->getTimestamp();
        if($in=='days')
        {
            $diff=$difference_time/(24*3600);
        }
        elseif($in=='hours')
        {
            $diff=$difference_time/(3600);
        }
        elseif($in=='minutes')
        {
            $diff=$difference_time/60;
        }
        return $diff;

       // return $diff->format("%R%a");
    }
}

/*if (!function_exists("the_diff")) {
    function the_diff($date1, $date2,$in='days')
    {
        
        $start_date = new DateTime($date1);
        $since_start = $start_date->diff(new DateTime($date2));
        if($in=='days')
        {
           return $since_start->days;
        }
        elseif($in=='years')
        {
             return $since_start->y;
        }
        elseif($in=='months')
        {
             return $since_start->y*12+$since_start->m;
        }
        elseif($in=='days')
        {
             return $since_start->y*12+$since_start->m;
        }
        elseif($in=='hours')
        {
             return $since_start->days * 24;
        }
        elseif($in=='minutes')
        {
            return  $since_start->days * 24 * 60 + $since_start->h * 60+$since_start->i;
        }
        elseif($in=='seconds')
        {
            return  $since_start->days * 24 * 60 + $since_start->h * 60+$since_start->i * 60+$since_start->s;
           
        }
        //return $diff;

       // return $diff->format("%R%a");
    }
}*/

if (!function_exists("date_years")) {
    function date_years($date1, $date2)
    {
        $date1 = date_create($date1);
        $date2 = date_create($date2);
        $diff = $date2->diff($date1);
        return $diff->y;
    }
}

/**
*
* Generates a random string
*
* @param <int> $length <The length of the string to be generated >
* @return <string> <The generated string>
*/
if(!function_exists("random_string"))
{
	function random_string($length=6,$range=FALSE) 
    {
		//If range not specified...use this
        if($range==FALSE)
		{
			$original_string = array_merge(range(0,9), range('a','z'), range('A', 'Z'));
		}
		else
		{
			$range=(!is_array($range) && strpos($range,"-"))?explode("-",$range):$range;
			$original_string=range($range[0],$range[1]);
		}
        $original_string = implode("", $original_string);
        return substr(str_shuffle($original_string), 0, $length);
    }
}

/**
*
* Prevents browser caching
*
* @note <Should be used before any content is rendered>
*/
if(!function_exists("prevent_cache"))
{
	function prevent_cache() 
    {
		// Date in the past
		header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header("Cache-Control: no-cache");
		header("Pragma: no-cache");
    }
}

/**
*
* Define Output for the browser
*
*@param $type <string> <Type of Output>
*@param $filename <string> <optional> <The name of the downloadable file>
* @note <Should be used before any content is rendered>
*/
if(!function_exists("render_as"))
{
	function render_as($type='pdf',$file_name=FALSE) 
    {
    	header("Content-type:application/".$type);
        // It will be called downloaded.pdf
        if($file_name!=FALSE)
        {
        	//Render a downloadable output
        	header("Content-disposition:attachment;filename=".$file_name.".".$type);	
        }        
    }
}

/**
*
* Sends a HTML Mail
*
* @param $from <string> <Sender>
* @param $to <string> <Recipient>
* @param $subject <string> <Email Subject>
* @param $message <string> <Message Content plain/html>
* @param $extras <array> <Array of Any Extras i.e. 'bcc','cc'>
* 
*/
if(!function_exists("send_html_mail"))
{
	function send_html_mail($from,$to,$subject=FALSE,$message=FALSE,$extras=array())
    {
        // Always set content-type when sending HTML email
        $headers = "MIME-Version: 1.0" . "\r\n";
        $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        // More headers
        $headers .= 'From: <'.$from.'>' . "\r\n";
        if($extras!=FALSE)
        {
            //$headers .= 'Cc: myboss@example.com' . "\r\n"; 
            $cc="";
            $bcc='';
            foreach($extras as $header_k=>$header_v)
            {
                if($header_k=="cc")
                {
                    foreach($header_v as $val)
                    {
                        $cc.=$val.",";
                    }
                }
                elseif($header_k=="bcc")
                {
                    foreach($header_v as $val)
                    {
                        $bcc.=$val.",";
                    }
                }
            }
            $headers .= 'Cc: '.substr($cc,0,-1). "\r\n";            
            $headers .= 'Bcc: '.substr($bcc,0,-1). "\r\n";      
        }
        mail($to,$subject,$message,$headers);
    }
}

if(!function_exists("pluralize"))
{
	function pluralize($string)
	{
		$plural = array(
				array( '/(quiz)$/i',               "$1zes"   ),
					array( '/^(ox)$/i',                "$1en"    ),
					array( '/([m|l])ouse$/i',          "$1ice"   ),
					array( '/(matr|vert|ind)ix|ex$/i', "$1ices"  ),
					array( '/(x|ch|ss|sh)$/i',         "$1es"    ),
					array( '/([^aeiouy]|qu)y$/i',      "$1ies"   ),
					array( '/([^aeiouy]|qu)ies$/i',    "$1y"     ),
					array( '/(hive)$/i',               "$1s"     ),
					array( '/(?:([^f])fe|([lr])f)$/i', "$1$2ves" ),
					array( '/sis$/i',                  "ses"     ),
					array( '/([ti])um$/i',             "$1a"     ),
					array( '/(buffal|tomat)o$/i',      "$1oes"   ),
					array( '/(bu)s$/i',                "$1ses"   ),
					array( '/(alias|status)$/i',       "$1es"    ),
					array( '/(octop|vir)us$/i',        "$1i"     ),
					array( '/(ax|test)is$/i',          "$1es"    ),
					array( '/s$/i',                    "s"       ),
					array( '/$/',                      "s"       )
			);

			$irregular = array(
					array( 'move',   'moves'    ),
					array( 'sex',    'sexes'    ),
					array( 'child',  'children' ),
					array( 'man',    'men'      ),
					array( 'person', 'people'   )
			);

			$uncountable = array(
					'sheep',
					'fish',
					'series',
					'species',
					'money',
					'rice',
					'information',
					'equipment'
			);

			// save some time in the case that singular and plural are the same
			if (in_array(strtolower($string), $uncountable))
				return $string;

			// check for irregular singular forms
			foreach ($irregular as $noun) {
				if (strtolower($string) == $noun[0])
					return $noun[1];
			}

			// check for matches using regular expressions
			foreach ($plural as $pattern) {
				if (preg_match($pattern[0], $string))
					return preg_replace($pattern[0], $pattern[1], $string);
			}

			return $string;
		}
	}

	/**
	* Singularize a string.
	* Converts a word to english singular form.
	*
	* Usage example:
	* {singularize "people"} # person
	*/
	function singularize ($params)
	{
		if (is_string($params))
		{
		$word = $params;
		}
		else if (!$word = $params['word'])
		{
		return false;
		}
		$singular = array (
		'/(quiz)zes$/i' => '\\1',
		'/(matr)ices$/i' => '\\1ix',
		'/(vert|ind)ices$/i' => '\\1ex',
		'/^(ox)en/i' => '\\1',
		'/(alias|status)es$/i' => '\\1',
		'/([octop|vir])i$/i' => '\\1us',
		'/(cris|ax|test)es$/i' => '\\1is',
		'/(shoe)s$/i' => '\\1',
		'/(o)es$/i' => '\\1',
		'/(bus)es$/i' => '\\1',
		'/([m|l])ice$/i' => '\\1ouse',
		'/(x|ch|ss|sh)es$/i' => '\\1',
		'/(m)ovies$/i' => '\\1ovie',
		'/(s)eries$/i' => '\\1eries',
		'/([^aeiouy]|qu)ies$/i' => '\\1y',
		'/([lr])ves$/i' => '\\1f',
		'/(tive)s$/i' => '\\1',
		'/(hive)s$/i' => '\\1',
		'/([^f])ves$/i' => '\\1fe',
		'/(^analy)ses$/i' => '\\1sis',
		'/((a)naly|(b)a|(d)iagno|(p)arenthe|(p)rogno|(s)ynop|(t)he)ses$/i' => '\\1\\2sis',
		'/([ti])a$/i' => '\\1um',
		'/(n)ews$/i' => '\\1ews',
		'/s$/i' => ''
		);
		$irregular = array(
		'person' => 'people',
		'man' => 'men',
		'child' => 'children',
		'sex' => 'sexes',
		'move' => 'moves'
		);
		$ignore = array(
		'equipment',
		'information',
		'rice',
		'money',
		'species',
		'series',
		'fish',
		'sheep',
		'press',
		'sms',
		);
		$lower_word = strtolower($word);
		foreach ($ignore as $ignore_word)
		{
		if (substr($lower_word, (-1 * strlen($ignore_word))) == $ignore_word)
		{
		return $word;
		}
		}
		foreach ($irregular as $singular_word => $plural_word)
		{
		if (preg_match('/('.$plural_word.')$/i', $word, $arr))
		{
		return preg_replace('/('.$plural_word.')$/i', substr($arr[0],0,1).substr($singular_word,1), $word);
		}
		}
		foreach ($singular as $rule => $replacement)
		{
		if (preg_match($rule, $word))
		{
		return preg_replace($rule, $replacement, $word);
		}
		}
		return $word;
	}


	if(!function_exists("is_email"))
	{
		function is_email($email) {
			return (preg_match("/[a-z\d.-_\+]+@[a-z\d.-_\+]+\.[a-z.]+/", $email) > 0)? true : false;
		}
	}


	if(!function_exists("curl_fetch"))
	{
		function curl_fetch($url) {
			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			return curl_exec($ch);
		}
	}

if(!function_exists("is_url"))
{
	function is_url($url) {
		if(filter_var(trim($url), FILTER_VALIDATE_URL))
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
	}
}

	function xml2object($data) {
        return json_decode(json_encode(xml2array($data)));
    }

    function xml2array($data) {
        $data = simplexml_load_string($data);
        return make_array($data);
    }

    function object2array($obj) {
        return make_array($obj);
    }

    function make_array($obj) {
        $arr = (array)$obj;
        if (empty($arr)) {
            $arr = "";
        } else {
            foreach ($arr as $key=>$value) {
                if (!is_scalar($value)) {
                    $arr[$key] = make_array($value);
                }
            }
        }
        return $arr;
    }

/**
 *
 * Get months
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("months")) {
    function months($key = false)
    {
        $months = array("1" => "January",
            "2" => "February",
            "3" => "March",
            "4" => "April",
            "5" => "May",
            "6" => "June",
            "7" => "July",
            "8" => "August",
            "9" => "September",
            "10" => "October",
            "11" => "November",
            "12" => "December");
        if ($key) {
            if (is_numeric($key)) {
                return isset($months[$key]) ? $months[$key] : false;
            } else {
                $months = array_flip($months);
                return isset($months[$key]) ? $months[$key] : false;
            }
        } else {
            return $months;
        }

    }
}

/**
 *
 * Get days
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("days")) {
    function days($key = false)
    {
        $days = array("1" => "Monday", "2" => "Tuesday", "3" => "Wednesday", "4" => "Thursday", "5" => "Friday", "6" => "Saturday", "7" => "Sunday");

        if ($key) {
            if (is_numeric($key)) {
                return isset($days[$key]) ? $days[$key] : false;
            } else {
                $days = array_flip($days);
                return isset($days[$key]) ? $days[$key] : false;
            }
        } else {
            return $days;
        }
    }
}

/**
 *
 * Get future years from a certain year with a set stepper
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("future_years")) {
    function future_years($period = 0, $from = false, $stepper = 1)
    {
        $year = ($from == FALSE) ? Date("Y") : $from;
        $years[date("Y")] = date("Y");
        for ($i = 0; $i < $period; $i++) {
            $year = $year + $stepper;
            $years[$year] = $year;
        }
        return $years;
    }
}

/**
 *
 * Get future years from a certain year with a set stepper
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("years")) {
    function years($past = 0, $future = 0, $from = false, $stepper = 1)
    {
        $year = ($from == FALSE) ? Date("Y") : $from;
        $future_years = future_years($future, $from, $stepper);
        $past_years = past_years($past, $from, $stepper);
        $years = $past_years + $future_years;
        return $years;
    }
}

/**
 *
 * Get future years from a certain year with a set stepper
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("numbers")) {
    function numbers($from = 0, $to = 0, $stepper = 1, $extension = "", $pad = array("", ""))
    {
        $i = $from;
        while ($i < $to) {
            $numbers[$i] = pad($i, $pad[0],$pad[1]) . $extension;
            $i += $stepper;
        }
        return $numbers;
    }
}

/**
 *
 * Get the past years by a set stepper
 *
 * @return <array> $months <Months of the Year>
 */
if (!function_exists("past_years")) {
    function past_years($period = 0, $from = FALSE, $stepper = 1)
    {
        $year = ($from == FALSE) ? Date("Y") : $from;
        $years[date("Y")] = date("Y");
        for ($i = 0; $i < $period; $i++) {
            $year = $year - $stepper;
            $years[$year] = $year;
        }
        return $years;
    }
}


/**
*
* Concatenate two strings
*
* @param <string> $str1 <Subject string>
* @param <string> $str2 <String to join with>
* @return <string> <Concatenated string>
*/
if(!function_exists("concat"))
{
	function concat($str1,$str2,$order="after")
	{
		return ($order==="after")?"{$str1} {$str2}":"{$str2} {$str1}";
	}
}

/**
*
* Parses a string and resolves it to a css path
*
* @param <string> $asset <Asset to parse>
* @param <path> $path <optional> <Path to the Asset>
* @return <string> <Concatenated string>
*/
if(!function_exists("build_css"))
{
	function build_css($asset,$path=FALSE)
	{
		$css=$path.(in_string(".css",$asset)?$asset:$asset.".css");
	}
}

/**
*
* Parses a string and resolves it to a css path
*
* @param <string> $asset <Asset to parse>
* @param <path> $path <optional> <Path to the Asset>
* @return <string> <Concatenated string>
*/
if(!function_exists("build_js"))
{
	function build_js($asset,$path=FALSE)
	{
		$css=$path.(in_string(".css",$asset)?$asset:$asset.".css");
	}
}


/**
*
* Registers a string into javascript script_tags
*
* @param <string> $asset <Asset to parse>
* @return <string> <Javascripted String>
*/
if(!function_exists("build_script"))
{
	function build_script($asset)
	{
		return "<script type='text/javascript'>\n".$asset."</script>\n";
	}
}



/**
*
* Parses a template with special characters to replace the text
*
* @param <string> $template <template string to parse>
* @param <string> <$vars> <Array of variables to look for>
* @param <string> <$temp_tags> <Array of temp tags actinga as holders>
*/
if(!function_exists("parse"))
{
	function parse($template,array $vars,array $temp_tags=array())
	{
		$temp_tags=empty($temp_tags)?["{","}"]:$temp_tags;	
		//Pad the vars with the templates
        $var_keys=array_keys($vars);
		array_walk($var_keys,function(&$v,&$k) use ($temp_tags){
			$v=pad($v,$temp_tags[0],$temp_tags[1]);
		});
		$vars=array_combine($var_keys,$vars);
		//Remove any unreplaced tags
        return strtr($template,$vars);
		//return between_replace(strtr($template,$vars),"",$temp_tags[0],$temp_tags[1]);
	}
}

/**
*
* Pad a string into some specified characters
*
* @param <string> $string <String to pad>
* @param <array> <$into> <Where to pad into>
* @param <string> <Padded string> 
*/
if(!function_exists("pad"))
{
	function pad($string,$before,$after)
	{
		return $before.$string.$after;
	}
}

/**
*
* Pad a string into some specified characters
*
* @param <string> $string <String to pad>
* @param <array> <$into> <Where to pad into>
* @param <string> <Padded string> 
*/
if(!function_exists("remove_ext"))
{
	function remove_ext($string,$doc_type)
	{
		$ext=".".$doc_type;
		return in_string($ext,$string && !empty($doc_type))?str_replace($ext,"",$string):$string;
	}
}

/**
*
* Pad a string into some specified characters
*
* @param <string> $string <String to pad>
* @param <array> <$into> <Where to pad into>
* @param <string> <Padded string> 
*/
if(!function_exists("add_ext"))
{
	function add_ext($string,$doc_type)
	{
		$ext=".".$doc_type;		
		return in_string($ext,$string) && !empty($doc_type)?$string:$string.$ext;
	}
}


/**
*
* Pad a string into some specified characters
*
* @param <string> $string <String to pad>
* @param <array> <$into> <Where to pad into>
* @param <string> <Padded string> 
*/
if(!function_exists("has_ext"))
{
    function has_ext($string,$doc_type)
    {
        $ext=".".$doc_type;     
        return in_string($ext,$string);
    }
}

/**
*
* Echo out a string
*
* @param <string> $string <String to be echoed>
*/
if(!function_exists("e"))
{
	function e($string="")
	{
		echo $string;
	}
}

/**
*
* Check is a variable is a valid closure
*
* @param <var> $var <Variable to check if is closure>
*/
if(!function_exists("is_closure"))
{
	function is_closure($var) {
	    return is_object($var) && ($var instanceof Closure);
	}
}

/**
*
* Implementation for Server-Side-Events for SSE HTTP Requests
*
* @param <data> $data <Data to be returned>
*/
if(!function_exists("sse"))
{
	function sse_header()
	{
		header("Content-Type: text/event-stream");
        header("Cache-Control: no-cache");
        header("Connection: keep-alive");
        //header('Access-Control-Allow-Origin: *');//optional

	}
}

function sse_send($id, $data,$event=false,$retry=100) {
        echo ($retry)?"retry:1\n":"";
        echo ($event)?"event: ".$event."\n":"";
        echo "id: $id\n";
        echo "data: $data\n\n";
        //ob_end_flush();
        flush();
        sleep(1);
    }

/**
*
* Implementation for Server-Side-Events for SSE HTTP Requests
*
* @param <data> $data <Data to be returned>
*/
if(!function_exists("sse_client"))
{
	function sse_client($source,$append_id="body",$scripted=TRUE)
	{
		return '<script>
		if(typeof(EventSource) !== "undefined"){
		    var source = new EventSource("'.$source.'");
		    source.onmessage = function(event) {
		        document.getElementById("result").innerHTML += event.data + "<br>";
		    };
		} else {
		    document.getElementById("result").innerHTML = "Sorry, your browser does not support server-sent events...";
		}
		</script>';
	}
}

function is_not_json($str)
{ 
    return json_decode($str) != null;
}


/**
*
* Get Array Elememts
*
* @param <array> $array <Array to check in>
* @param <string> $key <Key to check>
* @param <mixed> $default <Default to return>
* @return <mixed> <Element requested>
*/
if(!function_exists("arr"))
{
	function arr($key, $array_or_default, $default = false,$if_true_return=false)
	{
		return ($key)?(is_array($array_or_default) && array_key_exists($key, $array_or_default) ? ($if_true_return)?:$array_or_default[$key] : $default):false;
	}
}

// camelCase and StudlyCaps to snake_case
function camelToSnake($str)
{
    $str = preg_replace('/([a-z])([A-Z])/', '$1 $2', $str);
    $str = str_replace(' ', '_', strtolower($str));
    return $str;
}

// snake_case to camelCase
function snakeToCamel($str)
{
    $str = ucwords(str_replace('_', ' ', $str));
    $str = str_replace(' ', '', $str);
    return lcfirst($str);
}

// snake_case to StudlyCaps
function snakeToStudly($str)
{
    $str = ucwords(str_replace('_', ' ', $str));
    $str = str_replace(' ', '', $str);
    return ucfirst($str);
}

function pport_field_attributes()
{
    return array('params', 'model', 'modal', 'label', 'relationship','operators');
}

function is_pport_field_attribute($attribute)
{
    return in_array($attribute, pport_field_attributes());
}

function parse_model_params(&$options)
{
    $params = false;
    foreach (array("joins", "conditions", "select") as $param) {
        if (isset($options[$param])) {
            $params[$param] = $options[$param];
            unset($options[$param]);
        }
    }
    return $params;
}


function from_currency($amount)
{
    $amount = explode(".", $amount);
    $amount_whole = $amount[0];
    $amount_whole = str_replace(",", "", $amount[0]);
    $amount_decimal = (isset($amount[1])) ? $amount[1] : 0;
    return $amount_whole + ($amount_decimal * 0.01);
}

function rand_elem($array)
{
    return $array[rand(0, count($array) - 1)];
}

/**
 * Determine if this is a secure HTTPS connection
 *
 * @return  bool    True if it is a secure HTTPS connection, otherwise false.
 */
function is_secure()
{
    if (isset($_SERVER['HTTPS'])) {
        if ($_SERVER['HTTPS'] == 1 || $_SERVER['SERVER_PORT'] == HTTPS_PORT || $_SERVER['HTTPS'] == 'on') {
            return true;
        }
    }

    return false;
}

function curl_post($url, $fields = array(),$headers=false)
{
    $fields = (empty($fields)) ? $_POST : $fields;
    $url_string = urlify($fields);
    //open connection
    $ch = curl_init();
    //set the url, number of POST vars, POST data
	if($headers)
	{
		$headers = array();
		foreach($headers as $header)
		{
			$headers[] = $header;
		}



		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	}


    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_POST, count($fields));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $url_string);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_COOKIE, session_name() . '=' . session_id());
    //execute post
    $result = curl_exec($ch);
    //close connection
    curl_close($ch);
    
    return $result;
}

function urlify($url = "", $fields = false, $encode = true)
{
    $fields = is_array($url) ? $url : $fields;
    $fields_string = '';
    foreach ($fields as $key => $value) {
        if ($encode) {
            $value = urlencode($value);
        }
        $fields_string .= $key . '=' . $value . '&';
    }
    rtrim($fields_string, '&');
    if (is_array($url)) {
        return $fields_string;
    }
    return $fields_string;
}

function curl_get($url, $fields=[])
{
    $fields_string = urlify($url, $fields);
    $url=($fields_string!="")?$url.'?'.$fields_string:$url;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output;
}

function curl_json($url, $json_string,$type="POST")
{
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST,$type);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json_string);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          

        'Content-Type: application/json',                                                                                

        'Content-Length: ' . strlen($json_string))                                                                       

    );
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}


function at()
{
    return "&#64;";
}

function to_currency($number, $decimals = 2, $decpoint = ".", $separator = ",")
{
    return number_format((float)$number, $decimals, $decpoint, $separator);
}




function is_false($var)
{
    return ($var == FALSE) ? TRUE : FALSE;
}

function is_true($var)
{
    return ($var == TRUE) ? TRUE : FALSE;
}

function is_connected($url = 'www.google.com')
{

    $connected = @fsockopen($url, 80);
    //website, port  (try 80 or 443)
    if ($connected) {
        $is_conn = true; //action when connected
        fclose($connected);
    } else {
        $is_conn = false; //action in connection failure
    }
    return $is_conn;

}

function fetch_portlet($account, $portlet = 'comment', $params = array())
{
    return "<div id=\"" . $portlet . "_holder\"></div>
<script type=\"text/javascript\">
    /* * * CONFIGURATION VARIABLES * * */
    var account =$account;

    /* * * DON'T EDIT BELOW THIS LINE * * */
    (function() {
        var opencuil = document.createElement('script');
        opencuil.type = 'text/javascript';
        opencuil.async = true;
        opencuil.src = '//' + account + '.opencuil.com/comment.js';
        (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(opencuil);
    })();
</script>
<noscript>Please enable JavaScript to view the <a href=\"https://opencuil.com/?ref_noscript\" rel=\"nofollow\">" . $portlet . " powered by Opencuil.</a></noscript>";
}


function orm_to_array($data)
{
    $arr = array();
    if (is_object($data)) {
        //For single Records
        $arr = $data->to_array();
    } elseif (count($data) > 0) {
        foreach ($data as $row) {

            $arr[] = (is_object($row)) ? $row->to_array() : $row;
        }
    }

    return $arr;
}


function orm_to_json($data)
{
    return json_encode(orm_to_array($data));
}

function host()
{
    return $_SERVER['HTTP_HOST'];
}

function domain()
{
    explode($_SERVER['HTTP_HOST']);
}

function subdomain()
{
    $sections = explode(".", host());
    return $sections[0];
}

/**
 * Does what it says, converts camel case to underscore
 *
 * @param $string
 * @return string
 */

function camelCaseToUnderscore($string)
{
    return strtolower(preg_replace('/(?!^)[[:upper:]]/', '_\0', $string));
}


/**
 * String to udnerscore
 *
 * @param $string
 * @return string
 */

function toUnderscore($string)
{
    return strtolower(preg_replace('/([a-z])([A-Z])/', '$1_$2', $string));
}


function zip($folder_to_zip = '*', $backup_name = 'backup')
{
    system("zip -r " . $backup_name . ".zip " . $folder_to_zip);
}

if (!function_exists("the_past_mysql")) {
    function the_past_mysql($months)
    {
        return "NOW() - INTERVAL " . $months . " MONTH";
    }
}

if (!function_exists("the_past_mysql")) {
    function the_future_mysql($months)
    {
        return "NOW() + INTERVAL " . $months . " MONTH";
    }
}

if (!function_exists("get_layouts")) {
    function get_layouts($theme = false)
    {
        $theme = ($theme) ? $theme : Config::main('theme');
        // Open a directory, and read its contents
        $theme_dir = THEMES_PATH . $theme;
        if (is_dir($theme_dir)) {
            if ($dh = opendir($theme_dir)) {
                while (($file = readdir($dh)) !== false) {
                    $layout = remove_ext($file, 'php');
                    $layouts[strtolower($layout)] = $layout;
                }
                closedir($dh);
            }
        }
    }
}

if (!function_exists("directories")) {
    function directories($path = false)
    {
        return glob($path . "/*", GLOB_ONLYDIR);
    }
}

if (!function_exists("obj")) {
    function obj($key, $object, $default = "")
    {
        if (is_object($object)) {
            return (isset($object->{$key})) ? $object->{$key} : $default;
        } else {

            return (class_exists($object)&&isset($object::$$key)) ? $object::$$key : $default;
        }
    }
}

function round_currency($num, $decimal = 0.5)
{
    if ($decimal == 0.5) {
        round($num * 2) / 2;
    } elseif ($decimal == 0.05) {
        round($num * 2, 1) / 2;
    }

}


function unzip($zip_name, $to = '/')
{

    $zip = new ZipArchive;
    if ($zip->open($zip_name) === TRUE) {
        $zip->extractTo($to);
        $zip->close();
        return true;
    } else {
        return false;
    }

}


function unset_keys(array $input, $keys) {
    if (!is_array($keys))
        $keys = array($keys => 0);
    else
        $keys = array_flip($keys);

    return array_diff_key($input, $keys);
}

function replace_vars($field,&$f_v,$result, &$fields=array())
{
       
		if(is_scalar($f_v))
		{
			$f_v=array();
			$f_v['value']=$result->{$field};
		}
        


        

            if(isset($f_v['value']))
            {
                if(is_closure($f_v['value']))
                {
                    $f_v['value']=$f_v['value']($result);
                }
                else
                {
                    $f_v['value']=isset($result->{$field})&&!is_null($result->{$field})&&$result->{$field}!=""?$result->{$field}:$f_v['value'];
                }
            }
            else
            {
                
                $f_v['value']=!isset($result->{$field})?'':$result->{$field};
            }


        foreach($f_v as $f_key=>$f_value)
        {
            if(is_string($f_value))
            {

                $attr_vals = between_all($f_value, "{@", "}");

                if (!empty($attr_vals))
                {
                    foreach ($attr_vals as $k => $v)
                    {
                        if (isset($result->{$v}))
                        {
                            $f_v[$f_key] = str_replace('{@' . $v . '}', $result->{$v}, $f_value);
                        }
                        elseif(array_key_exists($v, $fields))
                        {
                            $replace_val = (isset($fields[$v]['value'])) ? $fields[$v]['value'] : '';
                            $f_v[$f_key] = str_replace('{@' . $v . '}', $replace_val, $f_value);
                        }
                    }
                }
                if(strpos($f_v[$f_key],'eval')||(isset($f_v['dynamic'])&&in_array($f_key,$f_v['dynamic'])))
                {

                    $result_string=str_replace('eval','',$f_v[$f_key]);
                    
                    $f_v[$f_key]=eval('return '.$result_string);
                }
            }
            elseif(is_closure($f_value))
            {
                $f_v[$f_key]=$f_value($result);
            }


        }
        return $f_v;
    }

    function array_first($array)
    {
        return array_values($array)[0];
    }

function is_json($str)
{
	if(is_string($str))
	{
		return json_decode($str) != null;
	}
	else
	{
		return false;
	}

}

function to_json($var)
{
	if(is_array($var))
	{
		$result=orm_to_json($var);

	}
	elseif(is_object($var))
	{
		$result=orm_to_json($var);

	}
	elseif(is_json($var))
	{
		$result=$var;
	}
	else
	{
		$result=NULL;
	}
	return $result;

}

function I($instance=false)
{
	return App::instance($instance);
}

function arr_eq($array1,$array2)
{
	return ($array1==$array2);
}

function arr_blacklist($keys, $array) {
	//Keys not in the needed keys are returned
	return array_diff_key($array, array_fill_keys($keys, 0));
}

function arr_whitelist($needed_keys, $data)
{
	return array_intersect_key($data, array_flip($needed_keys));
}

function json($data=false)
{
    header('content-type: application/json; charset=utf-8');
    if($data)
    {
        echo is_string($data)?$data:json_encode($data);
    }
}

function jsonp()
{
	/** JSON & JSONP **/
	//Sample request 1
	/**$.ajax({url: 'data.php', dataType:'jsonp'})*/
	//Sample Request 2
	/** $(document).ready(function() {
	var jq = $.getJSON('https://jira.atlassian.com/rest/api/latest/project?callback=?',
	function(data) {
	console.log("success");
	})
	.error(function() { console.log("error occurred"); });
	});**/

	header('content-type: application/json; charset=utf-8');
	# Set $data to something that will be serialized into JSON. You'll undoubtedly
	# have your own code for this.
	$data = array("some_key" => "some_value");

	# Encode $data into a JSON string.
	$json = json_encode($data);

	# If a "callback" GET parameter was passed, use its value, otherwise null. Note
	# that "callback" is a defacto standard for the JSONP function name, but it may
	# be called something else, or not implemented at all. For example, Flickr uses
	# "jsonpcallback" for their API.
	$jsonp_callback = isset($_GET['callback']) ? $_GET['callback'] : null;

	# If a JSONP callback was specified, print the JSON data surrounded in that,
	# otherwise just print out the JSON data.
	#
	# Specifying no callback param would print: {"some_key": "some_value"}
	# But specifying ?callback=foo would print: foo({"some_key": "some_value"})
	print $jsonp_callback ? "$jsonp_callback($json)" : $json;
}

/**
 *  An example CORS-compliant method.  It will allow any GET, POST, or OPTIONS requests from any
 *  origin.
 *
 *  In a production environment, you probably want to be more restrictive, but this gives you
 *  the general idea of what is involved.  For the nitty-gritty low-down, read:
 *
 *  - https://developer.mozilla.org/en/HTTP_access_control
 *  - http://www.w3.org/TR/cors/
 *
 */
function cors() {

	// Allow from any origin
	/**if (isset($_SERVER['HTTP_ORIGIN'])) {
		header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
		header('Access-Control-Allow-Credentials: true');
		header('Access-Control-Max-Age: 86400');    // cache for 1 day
	}

	// Access-Control headers are received during OPTIONS requests
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
			header("Access-Control-Allow-Methods: GET, POST, OPTIONS");

		if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
			header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

		//exit(0);
	}**/
	header('Access-Control-Allow-Origin: *');

	//header('Access-Control-Allow-Methods: GET, POST');

	header("Access-Control-Allow-Headers: X-Requested-With");

}

function truncate($string, $width, $etc = ' ..')
{
	$wrapped = explode('$trun$', wordwrap($string, $width, '$trun$', false), 2);
	return $wrapped[0] . (isset($wrapped[1]) ? $etc : '');
}

function chunk_params($gets,$ignore=false)
{
	for($i=0;$i<$ignore;$i++)
	{
		array_shift($gets);
	}
	$chunks = array_chunk($gets, 2);
	$chunked_data=array();
	foreach($chunks as $chunk)
	{
		if(isset($chunk[0])&&isset($chunk[1]))
		{
			$chunked_data[$chunk[0]]=$chunk[1];
		}
	}
	return $chunked_data;
}

function get($variable,$default=false)
{
	global $$variable;
	return isset($$variable)?$$variable:$default;
}


/**
 *  Function:   convert_number
 *
 *  Description:
 *  Converts a given integer (in range [0..1T-1], inclusive) into
 *  alphabetical format ("one", "two", etc.)
 *
 *  @int
 *
 *  @return string
 *
 */
function convert_number($number)
{
	if (($number < 0) || ($number > 999999999))
	{
		throw new Exception("Number is out of range");
	}

	$Gn = floor($number / 1000000);  /* Millions (giga) */
	$number -= $Gn * 1000000;
	$kn = floor($number / 1000);     /* Thousands (kilo) */
	$number -= $kn * 1000;
	$Hn = floor($number / 100);      /* Hundreds (hecto) */
	$number -= $Hn * 100;
	$Dn = floor($number / 10);       /* Tens (deca) */
	$n = $number % 10;               /* Ones */

	$res = "";

	if ($Gn)
	{
		$res .= convert_number($Gn) . " Million";
	}

	if ($kn)
	{
		$res .= (empty($res) ? "" : " ") .
			convert_number($kn) . " Thousand";
	}

	if ($Hn)
	{
		$res .= (empty($res) ? "" : " ") .
			convert_number($Hn) . " Hundred";
	}

	$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
		"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
		"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
		"Nineteen");
	$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
		"Seventy", "Eigthy", "Ninety");

	if ($Dn || $n)
	{
		if (!empty($res))
		{
			$res .= " and ";
		}

		if ($Dn < 2)
		{
			$res .= $ones[$Dn * 10 + $n];
		}
		else
		{
			$res .= $tens[$Dn];

			if ($n)
			{
				$res .= "-" . $ones[$n];
			}
		}
	}

	if (empty($res))
	{
		$res = "zero";
	}

	return $res;
}

function js_redirect($url)
{
	echo "<script type='text/javascript'> setTimeout(function(){window.location='".$url."'}, 3000);</script>";
}

function am_pm($time)
{
    date('h:i a', strtotime($time));
}


function scrape_from_table($htmlContent,$show_fields=false)
{
    if($show_fields)
    {
        $DOM = new DOMDocument();
    $DOM->loadHTML($htmlContent);
    
    $Header = $DOM->getElementsByTagName('th');
    $Detail = $DOM->getElementsByTagName('td');

    //#Get header name of the table
    foreach($Header as $NodeHeader) 
    {
        $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
    }
    //print_r($aDataTableHeaderHTML); die();

    //#Get row data/detail table without header name as key
    $i = 0;
    $j = 0;
    foreach($Detail as $sNodeDetail) 
    {
        $aDataTableDetailHTML[$j][] = trim($sNodeDetail->textContent);
        $i = $i + 1;
        $j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
    }
    //print_r($aDataTableDetailHTML); die();
    
    //#Get row data/detail table with header name as key and outer array index as row number
    for($i = 0; $i < count($aDataTableDetailHTML); $i++)
    {
        for($j = 0; $j < count($aDataTableHeaderHTML); $j++)
        {
            $aTempData[$i][$aDataTableHeaderHTML[$j]] = $aDataTableDetailHTML[$i][$j];
        }
    }
    $aDataTableDetailHTML = $aTempData; unset($aTempData);
    return $aDataTableDetailHTML;


        
    }

    $DOM = new DOMDocument();
        $DOM->loadHTML($htmlContent);
        
        $Header = $DOM->getElementsByTagName('th');
        $Detail = $DOM->getElementsByTagName('td');

        //#Get header name of the table
        foreach($Header as $NodeHeader) 
        {
            $aDataTableHeaderHTML[] = trim($NodeHeader->textContent);
        }
        //print_r($aDataTableHeaderHTML); die();

        //#Get row data/detail table without header name as key
        $i = 0;
        $j = 0;
        foreach($Detail as $sNodeDetail) 
        {
            $aDataTableDetailHTML[$j][] = trim($sNodeDetail->nodeValue);


            $imgs    = $sNodeDetail->getElementsByTagName('a');
            $has_img = $imgs->length > 0;

            if ($has_img) {     
                $has_alt = (bool) $imgs->item(0)->getAttribute("href");
                # img element has alt attribute?
                if ($has_alt) {
                    // do something...
                    $aDataTableDetailHTML[$j]['href']=$imgs->item(0)->getAttribute("href");
                }
            } else {
                // do something...
            }

            $i = $i + 1;
            $j = $i % count($aDataTableHeaderHTML) == 0 ? $j + 1 : $j;
        }

        return $aDataTableDetailHTML;
    
}

function scrape_from($string,$params)
{
	return Str::scrape_from($string,$params);
}


function scrape_and_group($string,$params,$params_children)
{
    return Str::scrape_and_group($string,$params,$params_children);
}

function no($value,$symbol=",")
{
    return str_replace($symbol,"",$value);
}

function get_quotient_and_remainder($divisor, $dividend) {
   return Math::get_quotient_and_remainder($divisor,$dividend);
}

 function substr_name($name,$type=FALSE)
    {
       return Str::substr_name($name,$type); 
    }

    function ago($date)
    {
        return Date::ago($date);
    }


    function file_fetch($file)
    {
        ob_start();
        include($file);
        $contents=ob_get_contents();

        ob_end_clean();

        return $contents;
    }

    

    

    function pp_wp_login( $user_id=false)
    {
        $user_id = ($user_id)?$user_id:Session::user('wp_user_id');
        $user = get_user_by( 'id', $user_id ); 
        
        if( $user ) {
            wp_set_current_user( $user_id, $user->user_login );
            wp_set_auth_cookie( $user_id );
            do_action( 'wp_login', $user->user_login );
        }
        else
        {
            pp_wp_register(false,false,true);
        }
    }

    function pp_wp_logout()
    {
        wp_logout();
    }


    function pp_wp_register($email_address=false,$user_name=false,$login=false)
    {
        if($email_address==false && $user_name==false)
        {
            
            $email_address=Session::user('email');
            $user_name=Session::user('email');
        }
       
        //Check if User Exists
        if( null == username_exists( $user_name ) ) {

           $id_or_errors = register_new_user($user_name, $email_address);
           //$user_id = wp_update_user( array( 'ID' => $id_or_errors, 'role' => 'admin') );
           $wp_user_object = new WP_User($id_or_errors);
            $wp_user_object->set_role('administrator');
           if ( !is_wp_error($id_or_errors) ) {
                 $session_user=Session::user();
                $user_id=$id_or_errors;
                $session_user->wp_user_id=$user_id;
                $session_user->save();
            }
            else
            {
                return false;
            }
        }
        else
        {
            $user = get_userdatabylogin($user_name);
            $user_id=$user->ID; 
        }
        

        if($login)
        {
            pp_wp_login($user_id);
        }
        else
        {
            return $user_id;
        }

        

    }


    function create_nonce()
    {

    }

    function verify_nonce()
    {
        
    }

    function is_url_encoded($message)
    {
        if ( urlencode(urldecode($message)) === $message){
            return true;
        } else {
           return false;
        }
    }

    function is_base_64_encoded($data)
    {
        //this will check whether data is base64 encoded or not
        if (base64_decode($data, true) == true)
        {          
           return true;          
        }
        else 
        {
           return false;
        }
    }


    function to_csv($all_results,$optional_name='data_export')
    {
        Csv::to_csv($all_results,$optional_name);
    }

    function force_wrap($str,$char=45)
    {
        return wordwrap($str,$char,"<br>\n",TRUE);
    }


    function dom_to_array($html,$tag='tr')
    {
        $data = array();
        $doc = new DOMDocument();
        $doc->loadHTML($html);
        $rows = $doc->getElementsByTagName($tag);
        foreach($rows as $row) {
            $values = array();
            foreach($row->childNodes as $cell) {
                $values[] = $cell->textContent;
            }
            $data[] = $values;
        }
    }

    function random_element($arr)
    {
        return  $arr[array_rand($arr, 1)];
    }



    function ordinal( $n )
    {
      return date('S',mktime(1,1,1,1,( (($n>=10)+($n>=20)+($n==0))*10 + $n%10) ));
    }

    function ordinalize($num)
    {
        return $num.ordinal($num);
    }

    function geocode($address,$google_map_api_key,$decode)
    {
        $result=file_get_contents('https://maps.googleapis.com/maps/api/geocode/xml?address='.$address.'&key='.$google_map_api_key);
        if($decode)
        {
            return json_decode($result);
        }
        return $result;
                    
    }

    function reverse_geocode($lat,$long,$google_map_api_key,$decode=false)
    {
        $result=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$lat.','.$long.'&key='.$google_map_api_key);
        if($decode)
        {
            return json_decode($result);
        }
        return $result;

    }

    //Check if image exists 
    //CREDIT : http://stackoverflow.com/posts/29714882/revisions
    function resource_exists($url){
       $headers=get_headers($url);
       return stripos($headers[0],"200 OK")?true:false;
    }


    function countries()
    {
        return Arr::countries();
    }

    function array_average($array) {
        return Arr::average();
    }

    function api_ai_response($speech,$displayText,$data=array(),$contextOut=array(),$source='opencuil')
    {
        header('Content-Type: application/json');
        echo json_encode(array('speech'=>$speech,'displayText'=>$displayText,'data'=>$data,'contextOut'=>$contextOut,'source'=>$source));
    }

   


    function alphabets()
    {
        return Str::alphabets();
    }

     function google_viewer($url,$attributes=false,$v1=false)
    {
        $attributes=build_attributes($attributes);
        if($v1)
        {
            return 'https://docs.google.com/viewer?embedded=true&url='.$url;
            exit();
            return '<iframe src="https://docs.google.com/viewer?embedded=true&url='.$url.'" frameborder="no" '.$attributes.'></iframe>';
        }
        return 'https://docs.google.com/viewerng/viewer?url='.$url;
            exit();
        return '<iframe src="https://docs.google.com/viewerng/viewer?url='.$url.'" frameborder="no" '.$attributes.'></iframe>';

    }

    function google_view($url,$attributes=false,$v1=false)
    {
        $attributes=build_attributes($attributes);
        if($v1)
        {
            header('location:https://docs.google.com/viewer?embedded=true&url='.$url);
            exit();
            return '<iframe src="https://docs.google.com/viewer?embedded=true&url='.$url.'" frameborder="no" '.$attributes.'></iframe>';
        }
        header('location:https://docs.google.com/viewerng/viewer?url='.$url);
            exit();
        return '<iframe src="https://docs.google.com/viewerng/viewer?url='.$url.'" frameborder="no" '.$attributes.'></iframe>';

    }

    

    function to_u($string,$search=' ')
    {
        return Str::to_u($string,$search);
    }

    function pad_with_string($data_array,$into=["['","']"])
    {
        return Str::pad_with_string($data_array,$into);
    }    

    function from_session($session_string_array='')
    {
            if(strpos($session_string_array,'->')!==false)
            {
                $data_array=explode('->',$session_string_array);
                $session_string_array=pad_with_string($data_array);
            }
            elseif(is_array($session_string_array))
            {
                $session_string_array=pad_with_string($session_string_array);
            }
            
            return eval("return "."isset($".'_SESSION'.$session_string_array.")?$".'_SESSION'.$session_string_array.":NULL;");
    }

    /**************/
    function  app_log($text,$log_file='storage/logs/mint.log')
    {
        Analog::handler (Analog\Handler\File::init ($log_file));
        Analog::log ($text);
    }

