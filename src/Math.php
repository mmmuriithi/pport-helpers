<?php
namespace pPort\Helpers;
/**
 * pPort Str Utility
 *
 * Provides various functions to interact/manipulate strings
 *
 * @author Martin Muriithi <martin@pporting.org>
 * @package system.utils
 * @category framework
 * @link http://www.pporting.org/docs/system/utils/string
 * @copyright Copyright (c) pPort Community Team
 * @license http://www.pporting.org/license
 * @version 1.0.0 <script version>
 * @since pPort Version 1.0.0
 *
 */

class Math
{
    public static function get_quotient_and_remainder($divisor, $dividend)
    {
         $quotient = (int)($divisor / $dividend);
        $remainder = $divisor % $dividend;
        return array( $quotient, $remainder );
    }
    
}
?>