<?php 

/**
*  Corresponding Class to test YourClass class
*
*  For each class in your library, there should be a corresponding Unit-Test for it
*  Unit-Tests should be as much as possible independent from other test going on.
*
*  @author yourname
*/
class StrTest extends PHPUnit_Framework_TestCase{
	
  
  /**
  * Check is_url 
  *
  *
  */
  public function testIsUrl(){
  	$var = new pPort\Str\Str;
  	$this->assertTrue($var->is_url("http://google.com") == true);
  	unset($var);
  }
  
}